       IDENTIFICATION DIVISION. 
       PROGRAM-ID. SHOPRECEIPT.
       AUTHOR. NUTTHJAREE.

       ENVIRONMENT DIVISION. 
       INPUT-OUTPUT SECTION. 
       FILE-CONTROL. 
           SELECT SHOP-RECEIPT-FILE ASSIGN TO "shop_receipts.dat"
              ORGANIZATION IS LINE SEQUENTIAL.
       DATA DIVISION. 
       FILE SECTION.
       FD  SHOP-RECEIPT-FILE.
       01  SHOP-HEADER.
           88 END-OF-SHOP-RECEIPT-FILE      VALUE HIGH-VALUE. 
           05 RECORD-TYPE-CODE              PIC X.
              88 IS-HEADER                  VALUE "H".
              88 IS-DETAIL                  VALUE "S".
           05 SHOP-ID                       PIC X(5).
           05 SHOP-LOCAT                    PIC X(30).    
       01  SHOP-RECEIPT.
           05 RECORD-TYPE-CODE              PIC X.
           05 ITEM-ID                       PIC X(8).
           05 ITEM-QTY                      PIC 9(3).           
           05 ITEM-PRICE                    PIC 9(3)V9(2).

       WORKING-STORAGE SECTION. 
       01  PRN-DETAIL-PRICE.
           05 FILLER PIC X(21)            VALUE "TOTAL SALES FOR SHOP".
           05 PRN-SHOP-ID                   PIC X(5).
           05 PRN-SHOP-TOTAL                PIC $$$,$$9.99.
       01 TOTAL-PRICE                       PIC 9(5)V9(2).

       PROCEDURE DIVISION .
       BEGIN.
           OPEN INPUT SHOP-RECEIPT-FILE
           PERFORM 001-READ-FILE
           PERFORM 001-PROCESS-HEADER UNTIL END-OF-SHOP-RECEIPT-FILE 
           CLOSE SHOP-RECEIPT-FILE 
           GOBACK 
           .
       001-PROCESS-HEADER.
            MOVE SHOP-ID TO PRN-SHOP-ID
            MOVE ZERO TO TOTAL-PRICE    
            PERFORM 001-READ-FILE
            PERFORM 001-PROCESS-SHOP-ITEM UNTIL IS-HEADER OR 
              END-OF-SHOP-RECEIPT-FILE
            MOVE TOTAL-PRICE TO PRN-SHOP-TOTAL
            DISPLAY PRN-DETAIL-PRICE
           .

       001-PROCESS-SHOP-ITEM.
           COMPUTE TOTAL-PRICE = TOTAL-PRICE + (ITEM-PRICE * ITEM-QTY )
           PERFORM 001-READ-FILE

           .
       001-READ-FILE.
            READ SHOP-RECEIPT-FILE  
               AT END SET END-OF-SHOP-RECEIPT-FILE TO TRUE
            END-READ
           .