       IDENTIFICATION DIVISION. 
       PROGRAM-ID. REPOROT-MEMBER.
       AUTHOR. NUTTHJAREE.

       ENVIRONMENT DIVISION. 
       INPUT-OUTPUT SECTION. 
       FILE-CONTROL. 
           SELECT MEMBER-FILE ASSIGN TO "member.dat"
              ORGANIZATION IS LINE SEQUENTIAL.
           SELECT MEMBER-REPORT-FILE ASSIGN TO "member.rpt"
              ORGANIZATION IS SEQUENTIAL.
       DATA DIVISION. 
       FILE SECTION.
       FD  MEMBER-FILE.
       01  MEMBER-DETAIL.
           88 END-OF-MEMBER-FILE      VALUE HIGH-VALUE. 
           05 MEMBER-ID               PIC X(5).
           05 MEMBER-NAME             PIC X(20).
           05 MEMBER-TYPE             PIC 9.
           05 MEMBER-GENDER           PIC X.
       FD  MEMBER-REPORT-FILE.
       01  PRN-LINE                   PIC X(44).

       WORKING-STORAGE SECTION.
       01 PAGE-HEADER.
           05 FILLER PIC X(44)
              VALUE "Rolling Greens Golf Club - Membership Report".
       01 PAGE-FOOTING.
           05 FILLER                  PIC X(15) VALUE SPACES.
           05 FILLER                  PIC X(7) VALUE "PAGE : ".
           05 PRN-PAGE-NUM            PIC Z9.
       01 COLUMN-HEADING              PIC X(50) 
           VALUE "MemberID  Member Name           Type Gender".
       01 MEMBER-DETAIL-LINE.
           05 FILLER                  PIC X VALUE SPACES.
           05 PRN-MEBER-ID            PIC X(5).
           05 FILLER                  PIC X(4) VALUE SPACES.
           05 PRN-MEMBER-NAME         PIC X(20).
           05 FILLER                  PIC XX VALUE SPACES.
           05 PRN-MEMBER-TYPE         PIC X.
           05 FILLER                  PIC X(4) VALUE SPACES.
           05 PRN-GENDER              PIC X.
       01 LINE-COUNT                  PIC 99 VALUE ZEROS.
           88 NEW-PAGE-REQUIRED       VALUE 40 THRU 99.
       01 PAGE-COUNT                  PIC 99 VALUE ZEROS.

       PROCEDURE DIVISION .
       BEGIN.
           OPEN INPUT MEMBER-FILE 
           OPEN OUTPUT MEMBER-REPORT-FILE 

           PERFORM 001-READ-FILE
           PERFORM 001-PROCESS-PAGE UNTIL END-OF-MEMBER-FILE 

           CLOSE MEMBER-FILE
           CLOSE MEMBER-REPORT-FILE
           GOBACK
           .
       001-READ-FILE.
           READ MEMBER-FILE 
              AT END SET END-OF-MEMBER-FILE TO TRUE
           END-READ 
           .
       001-PROCESS-PAGE.
           PERFORM 001-PROCESS-HEADER
           PERFORM 001-PROCESS-DETAIL UNTIL END-OF-MEMBER-FILE OR 
              NEW-PAGE-REQUIRED  
           PERFORM 001-PROCESS-FOOTER 
           .
       001-PROCESS-HEADER.
           MOVE ZERO TO LINE-COUNT
           
           WRITE PRN-LINE FROM PAGE-HEADER AFTER ADVANCING PAGE
           WRITE PRN-LINE FROM COLUMN-HEADING AFTER ADVANCING 2 LINE 
           ADD 3 TO LINE-COUNT
           ADD 1 TO PAGE-COUNT
           .
       001-PROCESS-DETAIL.
           MOVE MEMBER-ID TO PRN-MEBER-ID
           MOVE MEMBER-NAME TO PRN-MEMBER-NAME
           MOVE MEMBER-TYPE TO PRN-MEMBER-TYPE 
           MOVE MEMBER-GENDER TO PRN-GENDER
           WRITE PRN-LINE FROM MEMBER-DETAIL-LINE 
              AFTER ADVANCING 1 LINE
              ADD 1 TO LINE-COUNT  
           PERFORM 001-READ-FILE
           MOVE PAGE-COUNT TO PRN-PAGE-NUM  
           .
       001-PROCESS-FOOTER.
           WRITE PRN-LINE FROM PAGE-FOOTING AFTER ADVANCING 5 LINE
           .
