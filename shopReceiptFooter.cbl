       IDENTIFICATION DIVISION. 
       PROGRAM-ID. SHOPRECEIPTFOOTER.
       AUTHOR. NUTTHJAREE.

       ENVIRONMENT DIVISION. 
       INPUT-OUTPUT SECTION. 
       FILE-CONTROL. 
           SELECT SHOP-RECEIPT-FILE ASSIGN TO "shop_receipts_footer.dat"
              ORGANIZATION IS LINE SEQUENTIAL.
       DATA DIVISION. 
       FILE SECTION.
       FD  SHOP-RECEIPT-FILE.
       01  SHOP-HEADER.
           88 END-OF-SHOP-RECEIPT-FILE      VALUE HIGH-VALUE. 
           05 RECORD-TYPE-CODE              PIC X.
              88 IS-HEADER                  VALUE "H".
              88 IS-DETAIL                  VALUE "S".
              88 IS-FOOTER                  VALUE "F".
           05 SHOP-ID                       PIC X(5).
           05 SHOP-LOCAT                    PIC X(30).    
       01  SHOP-RECEIPT.
           05 RECORD-TYPE-CODE              PIC X.
           05 ITEM-ID                       PIC X(8).
           05 ITEM-QTY                      PIC 9(3).           
           05 ITEM-PRICE                    PIC 9(3)V9(2).
       01  SHOP-FOOTER.
           05 RECORD-TYPE-CODE              PIC X.
           05 RECORD-COUNT                  PIC 9(5).

       WORKING-STORAGE SECTION. 
       01  PRN-DETAIL-PRICE.
           05 FILLER PIC X(21)            VALUE "TOTAL SALES FOR SHOP".
           05 PRN-SHOP-ID                   PIC X(5).
           05 PRN-SHOP-TOTAL                PIC $$$,$$9.99.
       01  TOTAL-PRICE                       PIC 9(5)V9(2).
       01  ITEM-COUNT                        PIC 9(5).
       01  PRN-ERR-MESSGAE.
           05 FILLER               PIC X(16)   VALUE "ERROR ON SHOP : ". 
           05 PRN-ITEM-ID          PIC X(5).
           05 FILLER               PIC X(11)   VALUE ", RCOUNT = ".
           05 PRN-RECORD-COUNT     PIC 9(5).
           05 FILLER               PIC X(11)   VALUE ", ACOUNT = ".
           05 PRN-ACTUAL-COUNT     PIC 9(5).

       PROCEDURE DIVISION .
       BEGIN.
           OPEN INPUT SHOP-RECEIPT-FILE
           PERFORM 001-READ-FILE
           PERFORM 001-PROCESS-HEADER UNTIL END-OF-SHOP-RECEIPT-FILE 
           CLOSE SHOP-RECEIPT-FILE 
           GOBACK 
           .
       001-PROCESS-HEADER.
            MOVE SHOP-ID TO PRN-SHOP-ID
            MOVE ZERO TO TOTAL-PRICE    
            MOVE ZERO TO ITEM-COUNT 

            PERFORM 001-READ-FILE
            PERFORM 001-PROCESS-SHOP-ITEM UNTIL IS-FOOTER  OR 
              END-OF-SHOP-RECEIPT-FILE
            
            IF RECORD-COUNT = ITEM-COUNT 
              MOVE TOTAL-PRICE TO PRN-SHOP-TOTAL
              DISPLAY PRN-DETAIL-PRICE
            ELSE
              MOVE SHOP-ID TO PRN-ITEM-ID 
              MOVE RECORD-COUNT TO PRN-RECORD-COUNT
              MOVE ITEM-COUNT TO PRN-ACTUAL-COUNT
              DISPLAY PRN-ERR-MESSGAE 
            END-IF 
            PERFORM 001-READ-FILE 
           .

       001-PROCESS-SHOP-ITEM.
           COMPUTE TOTAL-PRICE = TOTAL-PRICE + (ITEM-PRICE * ITEM-QTY )
           ADD +1 TO ITEM-COUNT  
           PERFORM 001-READ-FILE
           .
       001-READ-FILE.
            READ SHOP-RECEIPT-FILE  
               AT END SET END-OF-SHOP-RECEIPT-FILE TO TRUE
            END-READ
           .